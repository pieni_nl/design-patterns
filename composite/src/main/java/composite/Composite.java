package composite;

import visitor.Visitor_Version_1;

import java.util.List;
import java.util.ArrayList;

/**
 * User: Pieter van der Meer
 * Date: Sep 29, 2009
 * Time: 9:09:38 PM
 */
public class Composite extends AbstractComponent {
    private List<AbstractComponent> children = new ArrayList<AbstractComponent>();

    public void addComponent(final AbstractComponent component) {
        children.add(component);
    }

    public List<AbstractComponent> getComponents() {
        return this.children;
    }

    public void operation() {
        for(AbstractComponent ac : children) {
            ac.operation();
        }
    }

    public void accept(Visitor_Version_1 visitorVersion1) {
        visitorVersion1.visit(this);
    }

}
