package composite;

import visitor.Visitor_Version_2;
import visitor.Visitor_Version_1;

/**
 * User: Pieter van der Meer
 * Date: Sep 29, 2009
 * Time: 9:09:21 PM
 */
public class Leaf_Version_2 extends AbstractComponent {
    public void operation() {
        System.out.println("Operation in leaf B");
    }

    public void accept(Visitor_Version_1 visitorVersion1) {
        if (visitorVersion1 instanceof Visitor_Version_2) {
            ((Visitor_Version_2) visitorVersion1).visit(this);
            return;
        }

        System.out.println("Hmmm what to do here!");

    }
}
