package composite;

import visitor.Visitor_Version_1;

/**
 * User: Pieter van der Meer
 * Date: Sep 29, 2009
 * Time: 9:08:45 PM
 */
public class Leaf_Version_1 extends AbstractComponent {
    public void operation() {
        System.out.println("Operation in Leaf A");
    }

    public void accept(Visitor_Version_1 visitorVersion1) {
        visitorVersion1.visit(this);
    }
}
