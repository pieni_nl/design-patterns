package composite;

import visitable.Visitable;

/**
 * User: Pieter van der Meer
 * Date: Sep 29, 2009
 * Time: 9:08:24 PM
 */
public abstract class AbstractComponent implements Visitable {
    public abstract void operation();
}
