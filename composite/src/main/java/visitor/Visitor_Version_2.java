package visitor;

import composite.Leaf_Version_2;

/**
 * User: Pieter van der Meer
 * Date: Sep 29, 2009
 * Time: 9:40:42 PM
 */
public interface Visitor_Version_2 extends Visitor_Version_1 {
    void visit(Leaf_Version_2 component);
}
