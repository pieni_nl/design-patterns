package visitor;

import composite.Leaf_Version_1;
import composite.Composite;

/**
 * User: Pieter van der Meer
 * Date: Sep 29, 2009
 * Time: 9:39:53 PM
 */
public interface Visitor_Version_1 {
    void visit(Leaf_Version_1 component);
    void visit(Composite component);
}
