package visitor;

import composite.AbstractComponent;
import composite.Composite;

/**
 * User: Pieter van der Meer
 * Date: Sep 29, 2009
 * Time: 9:41:13 PM
 */
public abstract class VisitorTemplate_Version1 implements Visitor_Version_1 {

    public void visit(Composite component) {
        System.out.println("Visiting Composite from template");
        for(AbstractComponent ac : component.getComponents()) {
            ac.accept(this);
        }
    }
}
