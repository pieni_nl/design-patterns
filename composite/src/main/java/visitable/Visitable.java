package visitable;

import visitor.Visitor_Version_1;

/**
 * User: Pieter van der Meer
 * Date: Sep 29, 2009
 * Time: 9:36:03 PM
 */
public interface Visitable {
    void accept(Visitor_Version_1 visitorVersion1);
}
