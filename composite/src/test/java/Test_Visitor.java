import visitor.VisitorTemplate_Version1;
import visitor.VisitorTemplate_Version_2;
import composite.Leaf_Version_1;
import composite.Composite;
import composite.Leaf_Version_2;
import org.junit.Test;

/**
 * User: Pieter van der Meer
 * Date: Sep 29, 2009
 * Time: 9:45:17 PM
 */
public class Test_Visitor {
    private class VisitorImplVersion1 extends VisitorTemplate_Version1 {

        public void visit(Leaf_Version_1 component) {
            System.out.println("Visiting a Leaf_Version_1 component");
        }
    }

    private class VisitorImpl_Version_2 extends VisitorTemplate_Version_2 {

        public void visit(Leaf_Version_1 component) {
            System.out.println("Visiting a Leaf_Version_1 component (version 2)");
        }

        public void visit(Leaf_Version_2 component) {
            System.out.println("Visiting a Leaf_Version_2 component (version 2)");
        }
    }

    @Test
    public void testVisitor() {
        Composite composite = new Composite();
        composite.addComponent(new Leaf_Version_1());
        composite.addComponent(new Leaf_Version_1());

        composite.accept(new VisitorImplVersion1());
    }

    @Test
    public void testVisitor_2() {
        Composite composite = new Composite();
        composite.addComponent(new Leaf_Version_1());
        composite.addComponent(new Leaf_Version_2());

        composite.accept(new VisitorImpl_Version_2());
    }
}
